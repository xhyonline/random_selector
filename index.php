<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>随机选项</title>
    <link rel="stylesheet" href="./layui/css/layui.css">
    <script src="./js/jquery.min.js"></script>
    <script src="./layui/layui.js"></script>
    <style>
        h1 {
            color: #009688;
            text-align: center;
            margin-top: 16px;
        }

        .layui-input-block {
            margin-left: 0;

        }

        .layui-form-item label {
            margin-top: 13px;
            margin-bottom: 13px;
            display: block;
        }

        .history-plan-icon, .add-icon {
            margin-top: 13px;
            float: right;
            font-weight: bold;
        }

        .save-icon {
            float: left;
            font-weight: bold;
            margin-top: 13px;
            clear: none;
        }

        .add-icon i {
            font-size: 18px;
        }

        .layui-btn-group {
            text-align: center;
            margin-top: 16px;
            display: block;

        }

        .layui-btn-group button {
            border: none !important;
        }

        .select-div {
            height: 300px;
            width: 100%;
            overflow: hidden;
            word-break: break-all;
            padding-top: 120px;
            overflow: hidden;
        }

        .select {
            margin-top: 16px;
            color: #FF5722;
            font-weight: bold;
            text-align: center;
            font-size: 30px;

        }

    </style>

</head>
<body>
<div class="layui-container">


    <!--    选择展示银幕-->
    <div class="screen" hidden>
        <h1>随机选择器</h1>
        <div class="select-div">
            <p class="select"></p>
        </div>
        <div class="layui-btn-group">
            <button class="layui-btn layui-btn-normal start">启动</button>
            <button class=" layui-btn layui-btn-danger stop">停止</button>
        </div>
    </div>
    <!--    -->
    <div class="select-main">
        <h1 class="">随机抉择工具</h1>
        <div class="layui-form-item history-plan-icon icon">
            <span>查看历史方案</span>
            <i class="layui-icon layui-icon-form"></i>
        </div>
        <div class="layui-form">
            <div class="layui-form-item">
                <label class="">请输入用户1的选项</label>
                <div class="layui-input-block">
                    <input type="text" name="" placeholder="例如:鸡公煲" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="">请输入用户2的选项</label>
                <div class="layui-input-block">
                    <input type="text" name="" placeholder="例如:盖浇饭" autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>

        <button type="button" class="layui-btn layui-btn-fluid random-btn">进入随机抉择</button>
        <div class="layui-form-item add-icon icon">
            <span>继续添加选项</span>
            <i class="layui-icon layui-icon-add-circle"></i>
        </div>
        <div class="layui-form-item save-icon icon">
            <span>保存方案</span>
            <i class="layui-icon layui-icon-note"></i>
        </div>
    </div>
    <input type="hidden" value="" class="hidden">
</div>
</body>
<script>
    var layer, form
    var storage = window.localStorage;
    layui.use(['layer', 'form'], function () {
        layer = layui.layer;

        form = layui.form;
    });
    $(function () {
        var item = $(".select-main input").length
        $(".add-icon").click(function () {
            var item = $(".select-main input").length
            item++
            var form = ` <div class="layui-form-item">
                <label class="">请输入用户${item}的选项</label>
                <div class="layui-input-block">
                    <input type="text" name="" placeholder="请输入用户${item}的选项" autocomplete="off" class="layui-input">
                </div>
            </div>`
            $(".layui-form").append(form)
        });


        var select_arr = [];

        //随机函数
        function random() {
            var i = 0;
            var length = select_arr.length
            $(".screen").show();
            $(".select-main").hide();
            start_random = setInterval(function () {
                $(".select").text(select_arr[i])
                if (i == length - 1) {
                    i = 0
                } else {
                    i++
                }
            }, 100)
        }

        $(".random-btn").click(function () {
            var flag = 0
            $(".layui-form input").each(function () {
                var user_select = $(this).val();
                if (user_select != "") {
                    flag++
                    select_arr.push(user_select)
                }
            });
            if (flag < 2) {
                select_arr = []
                layer.msg('至少要选择两个选项哦');
                return false;
            }

            $(".start").hide()
            random()
        });
        //保存方案按钮
        $(".save-icon").click(function () {
            var input = ` <div class="layui-form-item plan-form">
                        <label class="">请为方案命名</label>
                        <div class="layui-input-block">
                        <input type="text" name="" placeholder="例如:今晚吃什么?" autocomplete="off" class="layui-input">
                        </div>
                        </div>`
            layer.open({
                content: input
                , btn: ['确定', '取消']
                , yes: function (index, layero) {
                    var val = $(".plan-form input").val()
                    if (val.length == 0) {
                        layer.msg("方案名不能为空", {type: 1}) //加上type1保证open和msg能够共存,否则一个弹出.另一个会关闭
                    } else {
                        var flag = 0
                        select_arr = []
                        $(".layui-form input").each(function () {
                            var user_select = $(this).val();
                            if (user_select != "") {
                                flag++
                                select_arr.push(user_select)
                            }
                        });
                        if (flag < 2) {
                            select_arr = []
                            layer.msg('方案至少要包含两个选项不能为空喔');
                            return false;
                        }

                        json = {plan_name: val, plan: select_arr}

                        if (storage["data"] != undefined) {
                            var obj = JSON.parse(storage["data"])
                            //判断方案名是否重复
                            $.each(obj.plans,function (i,plan) {
                                res= plan.plan_name== val ? true :false
                                if (res){
                                    return false
                                }
                            })

                            if (res){
                                layer.msg("方案命名重复,请换一个名字", {type: 1})
                                return false;
                            }


                            obj.plans.push(json)
                            jsonStr = JSON.stringify(obj);
                            storage["data"] = jsonStr
                        } else {
                            var arr = []
                            arr.push(json)
                            var pushJson = {}
                            pushJson.plans = arr
                            var jsonStr = JSON.stringify(pushJson);
                            storage["data"] = jsonStr
                        }

                        layer.msg('保存成功');
                        layer.close(index)
                    }
                }
                , cancel: function () {

                },  success: function(layero, index){
                   $(".plan-form input").focus()
                }

            });
        })

        //查看历史方案按钮
        $(".history-plan-icon").click(function () {


            if (storage.data==undefined){
                layer.msg("当前暂无任何保存的方案,请先添加方案")
                return false

            }
            var obj = JSON.parse(storage["data"])
            var option = ""
            $.each(obj, function (index, itemList) {
                $.each(itemList, function (index, item) {

                    option += `<input type="radio" name="history_plans" title="${item.plan_name}" lay-skin="primary" class="radio" value="${item.plan_name}">`
                })
            });
            var select = `
  <form class="layui-form" action="">
 <div class="layui-form-item">

  <div class="layui-input-block">
        ${option}
        </div>
              </div>
              </form>
         `
            layer.open({
                content: select
                , title: "请选择历史方案"
                ,btn: ['确定','清空方案']
                ,maxWidth:250
                , yes: function (index, layero) {

                    var res = $(".radio").is(":checked")
                    if (res) {
                        var plan_name = $(".radio:checked").val();
                        var plan = []
                        flag=0
                        $.each(obj, function (i, valList) {
                            $.each(valList, function (i, val) {

                                if (val.plan_name == plan_name) {
                                    plan = val.plan

                                    flag=1
                                    return false
                                }
                            })
                            if(flag==1){
                                return false
                            }
                        });

                        //遍历
                        if (plan.length==2){

                            //判断有几个各自
                            var length =$(".select-main input").length
                            while (length>2){
                                $(".select-main .layui-form-item").eq(length-1).remove()
                                length =$(".select-main input").length
                            }



                            $(".select-main input").each(function (i) {
                                var val=plan[i]
                                $(this).val(val)
                            })
                        }else{
                            var hidden_val=$(".hidden").val()
                            var length =$(".select-main input").length
                            while (length>2){
                                $(".select-main .layui-form-item").eq(length-1).remove()
                                length =$(".select-main input").length
                            }
                            if (hidden_val==plan_name){

                                return false

                            }
                            for (i=3;i<=plan.length;i++){
                                var form = ` <div class="layui-form-item">
                                <label class="">请输入用户${i}的选项</label>
                                <div class="layui-input-block">
                                    <input type="text" name="" placeholder="请输入用户${item}的选项" autocomplete="off" class="layui-input">
                                </div>
                            </div>`
                                $(".select-main .layui-form").append(form)
                            }

                            $(".select-main input").each(function (i) {
                                var val=plan[i]
                                $(this).val(val)
                            })

                        }

                        $(".hidden").val(plan_name)

                        layer.msg("切换成功")

                    }
                } , btn2: function (index,layero) {
                    layer.confirm('您确定要清空历史方案？', {
                        btn: ['确定','取消'] //可以无限个按钮
                    }, function(index, layero){
                        storage.clear();
                        layer.msg("清除成功")
                    }, function(index){

                    });
                }

            })

            form.render();


        })


        $(".stop").click(function () {
            clearInterval(start_random)
            $(".start").show();
            $(this).hide();
        })
        $(".start").click(function () {
            $(".stop").show();
            $(this).hide();
            random()
        })

        $("input").on('blur',function(){
            window.scroll(0,0);
        });
    })
</script>
</html>